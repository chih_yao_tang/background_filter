#include "Extract_ball_candidate.h"
#include <iostream>
#include <fstream>
#include <sstream>

void Extract_ball_candidate::set_element_size(int element)
{
	this->element_size_ = element;
	int rect_len = element_size_*element_size_ + 1;
	*(this->element_) = element_mat_generator(rect_len, element_size_);
}

void Extract_ball_candidate::set_camera_focal(double f)
{
	camera_focal_ = f;
}
void Extract_ball_candidate::set_ball_radius(double r)
{
	ball_radius_ = r;
}

void Extract_ball_candidate::compute(cv::Mat* src, cv::Mat* depth, cv::Mat* rst)
{
	pairs_->clear();
	cv::Mat* mat_binary_ = src;
	vector<vector<cv::Point> > contours;
	vector<cv::Vec4i> hierarchy;
	cv::findContours( *mat_binary_, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	*contour_ = cv::Mat(480, 640, CV_8UC3, cv::Scalar(0));

	//std::cout << "contours.size() " <<  contours.size() << std::endl;
	for( int k = 0; k < contours.size(); k++ )
	{
		double _area = cv::contourArea( contours[k] );

		// Get the moments
		vector<cv::Moments> mu(contours.size() );
		mu[k] = moments( contours[k], false );
				
		//  Get the mass centers:
		vector<cv::Point2f> mc( contours.size() );
		if(_area > 0){
			mc[k] = cv::Point2f( mu[k].m10/mu[k].m00 , mu[k].m01/mu[k].m00 ); 
			
			unsigned short center_depth (depth->at<unsigned short>(floor(mc[k].y), floor(mc[k].x)));
			

			float radius_on_image = (ball_radius_*camera_focal_) / center_depth;
			double est_area = get_area_(radius_on_image);
			if( _area >= 0.8* est_area && _area < 5*est_area && center_depth <= 4000 )
			{
				std::cout << " - " << center_depth << ", " << _area << ", " << est_area << std::endl;
				cv::Scalar color = cv::Scalar( rng_.uniform(0, 255), rng_.uniform(0,255), rng_.uniform(0,255) );
				drawContours( *contour_, contours,k, color, 2, 8, hierarchy, 0, cv::Point() );
				cv::Point2f pPair(mc[k]);
				pairs_->push_back(pPair);
				depth_value_->push_back(center_depth);
			}
		}
	}
}

vector<cv::Point2f>* Extract_ball_candidate::get_cadidate_points()
{
	return pairs_;
}
vector<unsigned short>* Extract_ball_candidate::get_depth_value()
{
	return depth_value_;
}

cv::Mat& Extract_ball_candidate::element_mat_generator(int rect_len, int element_size)
{
	return  cv::getStructuringElement( cv::MORPH_RECT, cv::Size( rect_len, rect_len), cv::Point( element_size, element_size) );
}
double Extract_ball_candidate::get_area_(double radius)
{
	return (radius*radius)*PI;
}

cv::Mat* Extract_ball_candidate::get_contour_image()
{
	return contour_;
}



