#ifndef _EXTRACT_BALL_CENTER_H_
#define _EXTRACT_BALL_CENTER_H_

#define PI 3.14159265

// OpenCV Header
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

class extract_ball_center
{
public:
	extract_ball_center():

			rng_(12345),
			record_eable_(false)
	{};
	~extract_ball_center();
	void set_element_size(int element);
	void set_ball_radius(float r);
	void set_camera_focal(float f);
	void compute(cv::Mat* src, cv::Mat* depth, cv::Mat rst);

private:
	cv::Mat& element_mat_generator(int rect_len, int element_size);
	double get_area_(double radius);

private:
	bool record_eable_;
	int element_size_;
	float ball_radius_;
	float camera_focal_;
	cv::RNG rng_;
	cv::Mat* element_;

	vector<std::pair<int, cv::Point2f>> pairs_;
	vector<unsigned short> depth_value_;
	
};


#endif _EXTRACT_BALL_CENTER_H_