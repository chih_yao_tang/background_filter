﻿#include <OpenNI.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>


#include "depth_background_filter.h"
#include "extract_ball_candidate.h"

#define PI 3.14159265
using namespace std;


openni::PlaybackControl*		g_pPlaybackControl;
vector<openni::Device*>			g_vpDevices;
vector<openni::VideoStream*>	g_vpDepthStream;
vector<openni::Recorder*>		g_vpRecorder;
vector<std::pair<int, double>>	g_vFrameTime;
vector<int>	g_vIndex;
vector<openni::VideoFrameRef*>	g_frameRef;
vector<std::pair<int, cv::Point2f>> g_cadidates;
vector<float> g_cadidates_depth;

int main(int argc, char** argv)
{
	string dir(argv[1]);

	// 1. Initialize
	openni::Status rc = openni::STATUS_OK;
	openni::OpenNI::initialize();


	// 2. Open the oni file
	openni::Device* device = new openni::Device();
	openni::VideoStream* depthStream = new  openni::VideoStream();
		
	stringstream ssin;
	ssin << dir << ".oni" ;

	cout << "* open file ... " << ssin.str() << endl;
	rc = device->open(ssin.str().c_str());
	
	// 3a. Get number of frame
	g_pPlaybackControl = device->getPlaybackControl();
	depthStream->create(*device, openni::SENSOR_DEPTH);
	int  iNumberOfFrames = g_pPlaybackControl->getNumberOfFrames(*depthStream);
	cout << iNumberOfFrames << endl;
	g_frameRef.resize(iNumberOfFrames);
	g_vIndex.resize(iNumberOfFrames);

	// 3b. Get the frame from the oni file
	openni::VideoFrameRef depthFrame;
	openni::VideoMode mMode;
	mMode.setResolution( 640, 480 );
	mMode.setFps( 30 );
	mMode.setPixelFormat( openni::PIXEL_FORMAT_DEPTH_1_MM );
	depthStream->setVideoMode(mMode);
	depthStream->start();

	// reorder vedio frames
	for(int i = 0; i < iNumberOfFrames; i++)
	{
		openni::VideoFrameRef* tmpFrame = new openni::VideoFrameRef();
		depthStream->readFrame(tmpFrame);
		g_frameRef.at(tmpFrame->getFrameIndex()-1) = tmpFrame;

		g_vIndex.at(i) = tmpFrame->getFrameIndex();
	}
	depthFrame = *g_frameRef.at(0);


	cv::Mat image_background_model(depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, cv::Scalar(0));;
	cv::Mat image_countour(depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, cv::Scalar(0));
	Background_filter* background_filter = new Background_filter();
	background_filter->set_element_size(3);
	cout << "* background_filter " << endl;
	Extract_ball_candidate* extract_ball_candidate = new Extract_ball_candidate();
	extract_ball_candidate->set_ball_radius(35.0);
	extract_ball_candidate->set_camera_focal(526.119);
	extract_ball_candidate->set_element_size(2);// morphing element size
	cout << "* extract_ball_candidate " << endl;
	stringstream file_name;

	for (int i = 0; i < iNumberOfFrames ; i++)
	{
		depthFrame = *g_frameRef.at(i);
		cout << "* Frame : "  << i << endl;
		cv::Mat image_depth( depthFrame.getHeight(), depthFrame.getWidth(), CV_16UC1, (void*)depthFrame.getData() );
		background_filter->set_input_image(&image_depth);
		background_filter->set_model_image(&image_background_model);
		background_filter->compute();

		// close training background
		if(i == 40)
			background_filter->set_training_switch(false);

		if( i > 40) 
		{
			// extract cadidate points from binary image
			extract_ball_candidate->compute(background_filter->get_diff_grayscale(), &image_depth, &image_countour);
			
			// reocrd cadidate points and depth in the frame
			vector<cv::Point2f>* cadidates_in_frame =  extract_ball_candidate->get_cadidate_points();
			vector<unsigned short>* cadidates_depth_in_frame = extract_ball_candidate->get_depth_value();
			for(int num = 0; num < cadidates_in_frame->size(); ++num)
			{
				g_cadidates.push_back(std::pair<int, cv::Point2f>(i, cadidates_in_frame->at(num)) );
				g_cadidates_depth.push_back(cadidates_depth_in_frame->at(num) );
			}

		}

		// record experiment
		//file_name.str("");
		//file_name << i << ".png";
		//cv::imwrite(file_name.str().c_str(), *(background_filter->get_diff_binary() ));


		//cv::imshow("NoiseMask", *(background_filter->get_noise_mask()) );
		//cv::imshow("Diff", *(background_filter->get_diff()));
		//cv::imshow("BackgroundModel", *(background_filter->get_model_image()));
		//cv::imshow("image_binary", image_binary);
		cv::imshow("image_countour", *(extract_ball_candidate->get_contour_image()));
		cv::waitKey(33);		
	}


	// record cadidates to text file
	stringstream ss;
	ss << dir << "_CandidatePoints"  << ".txt";
	std::cout << ss.str() << std::endl;
	fstream fp;
	fp.open(ss.str().c_str(), ios::out);

	for (int i = 0; i < g_cadidates.size(); ++i)
	{
		cout << " - " << i << endl;
		fp << g_cadidates.at(i).first << "\t" 
		   << g_cadidates.at(i).second.x << "\t" 
		   << g_cadidates.at(i).second.y << "\t" 
		   << g_cadidates_depth.at(i) << "\n";
	}
	fp.close();


	depthStream->stop();
	openni::OpenNI::shutdown();
	return 0;
}
