#ifndef _DEPTH_BACKGROUND_FILTER_H_
#define _DEPTH_BACKGROUND_FILTER_H_

// OpenCV Header
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//class Mat;

class Background_filter
{
public:
	Background_filter() : 
					learning_rate_(0.1) , 
					stop_training_model_(0), 
					mfb_(0.0000285),
					training_iter_(0),
					frame_count_(0),
					frequency_cleaner_(false),
					mat_diff_				(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0))),
					mat_diff_gray_			(new cv::Mat(480, 640, CV_8U   , cv::Scalar(0))),
					mat_noise_mask_			(new cv::Mat(480, 640, CV_8UC1 , cv::Scalar(0))),
					mat_noise_mask_dilate_	(new cv::Mat(480, 640, CV_8UC1 , cv::Scalar(0))),
					mat_noise_frequency_	(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0))),
					mat_noise_range_max_	(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0))),
					mat_noise_range_min_	(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0))),
					element_				(new cv::Mat(480, 640, CV_8UC1 , cv::Scalar(0))),
					noise_max_calcutate_times_(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0))),
					noise_min_calcutate_times_(new cv::Mat(480, 640, CV_16UC1, cv::Scalar(0)))
					
	{
	};
	~Background_filter();
	
	void set_input_image( cv::Mat* src );
	void set_model_image( cv::Mat* src );
	void set_learning_rate( double LR );
	void set_input_mask( cv::Mat* mask, cv::Mat* frequency );
	void set_training_switch( bool training_switch );
	void set_training_iteration(int times);
	void set_element_size(int element);
	cv::Mat* get_model_image();
	cv::Mat* get_noise_mask();
	cv::Mat* get_diff();
	cv::Mat* get_diff_grayscale();
	cv::Mat* get_diff_binary();
	cv::Mat* get_noise_frequency();
	void compute();
private:
	
	void filter_by_depth();
	void filter_by_difference();
	void model_generator();
	void noise_mask_generator();
	void noise_mask_application();
	void evaluate_threshold();
	cv::Mat element_mat_generator(int rect_len, int element_size);
private:
	cv::Mat* mat_src_ ;
	cv::Mat* mat_model_;
	cv::Mat* mat_diff_;
	cv::Mat* mat_diff_binary_;
	cv::Mat* mat_diff_gray_;
	cv::Mat* mat_noise_mask_;
	cv::Mat* mat_noise_mask_dilate_;
	cv::Mat* mat_noise_frequency_;
	cv::Mat* mat_noise_range_max_;
	cv::Mat* mat_noise_range_min_;
	cv::Mat* element_;
	cv::Mat* noise_max_calcutate_times_;
	cv::Mat* noise_min_calcutate_times_;

	double learning_rate_;
	bool stop_training_model_;
	bool frequency_cleaner_;
	
	// (Accuracy and Resolution of Kinect Depth Data for Indoor
	// Mapping Applications)
	// equation error variable
	double mfb_;
	// temp parameter
	short* ptr_src_;
	short* ptr_model_;
	short* ptr_diff_;
	uchar* ptr_mask_;
	short* ptr_frequency_;
	short* ptr_range_max_;
	short* ptr_range_min_;
	short* ptr_range_max_times;
	short* ptr_range_min_times;
	int ptr_pos_;
	double parm_threshold_;
	int element_size_;
	int training_iter_;
	int frame_count_;

};

#endif _DEPTH_BACKGROUND_FILTER_H_