#include "extract_ball_center.h"



void extract_ball_center::set_element_size(int element)
{
	this->element_size_ = element;
	int rect_len = element_size_*element_size_ + 1;
	*(this->element_) = element_mat_generator(rect_len, element_size_);
}

void extract_ball_center::set_camera_focal(float f)
{
	camera_focal_ = f;
}
void extract_ball_center::set_ball_radius(float r)
{
	ball_radius_ = r;
}

void extract_ball_center::compute(cv::Mat* src, cv::Mat* depth, cv::Mat rst)
{
	cv::Mat mat_binary_(src->rows, src->cols, CV_16UC1, cv::Scalar(0));
	vector<vector<cv::Point> > contours;

	cv::threshold(*src, mat_binary_,1, 255, cv::THRESH_BINARY);
	vector<cv::Vec4i> hierarchy;
	cv::findContours( mat_binary_, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
	cv::Mat drawing = cv::Mat::zeros( mat_binary_.size(), CV_8UC3 );
	for( int k = 0; k < contours.size(); k++ )
	{
		double _area = cv::contourArea( contours[k] );
		// Get the moments
		vector<cv::Moments> mu(contours.size() );
		mu[k] = moments( contours[k], false );
				
		//  Get the mass centers:
		vector<cv::Point2f> mc( contours.size() );
		mc[k] = cv::Point2f( mu[k].m10/mu[k].m00 , mu[k].m01/mu[k].m00 ); 
		
		unsigned short center_depth (depth->at<unsigned short>(floor(mc[k].y), floor(mc[k].x)));
		float radius_on_image = (ball_radius_*camera_focal_) / center_depth;

		if( _area >= 0.9* get_area_(radius_on_image) && center_depth <= 4000 )
		{
			cv::Scalar color = cv::Scalar( rng_.uniform(0, 255), rng_.uniform(0,255), rng_.uniform(0,255) );
			drawContours( drawing, contours,k, color, 2, 8, hierarchy, 0, cv::Point() );
			if (record_eable_)
			{
				std::pair<int, cv::Point2f> pPair(0, mc[k]);
				pairs_.push_back(pPair);
				depth_value_.push_back(center_depth);//round(mc[k].x),round(mc[k].y)
			}
		}
	}
}

cv::Mat& extract_ball_center::element_mat_generator(int rect_len, int element_size)
{
	return  cv::getStructuringElement( cv::MORPH_RECT, cv::Size( rect_len, rect_len), cv::Point( element_size, element_size) );
}
double extract_ball_center::get_area_(double radius)
{
	return (radius*radius)*PI;
}
