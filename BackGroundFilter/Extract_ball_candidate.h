#ifndef _EXTRACT_BALL_CANDIDATE_H_
#define _EXTRACT_BALL_CANDIDATE_H_

#define PI 3.14159265

// OpenCV Header
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

class Extract_ball_candidate
{
public:
	Extract_ball_candidate():

			rng_(12345),
			record_eable_(true),
			element_(new cv::Mat(480, 640, CV_8UC1, cv::Scalar(0))),
			contour_(new cv::Mat(480, 640, CV_8UC3)),
			pairs_(new vector<cv::Point2f>),
			depth_value_(new vector<unsigned short>)

	{};
	~Extract_ball_candidate();
	void set_element_size(int element);
	void set_ball_radius(double r);
	void set_camera_focal(double f);
	cv::Mat* get_contour_image();
	void compute(cv::Mat* src, cv::Mat* depth, cv::Mat* rst);
	vector<cv::Point2f>* get_cadidate_points();
	vector<unsigned short>* get_depth_value();
	void reocrd_cadidate_points(const char* dir);
private:
	cv::Mat& element_mat_generator(int rect_len, int element_size);
	double get_area_(double radius);

private:
	bool record_eable_;
	int element_size_;
	float ball_radius_;
	float camera_focal_;
	cv::RNG rng_;
	cv::Mat* element_;
	cv::Mat* contour_;
	vector<cv::Point2f>* pairs_;
	vector<unsigned short>* depth_value_;
	
};


#endif _EXTRACT_BALL_CANDIDATE_H_