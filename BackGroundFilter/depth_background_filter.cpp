#include "depth_background_filter.h"
#include <iostream>

void Background_filter::set_learning_rate(double LR)
{
	this->learning_rate_ = LR;
}

void Background_filter::set_training_switch(bool training_switch)
{
	this->stop_training_model_ = ~training_switch;
}

void Background_filter::set_training_iteration(int times)
{
	this->training_iter_ = times;
}

void Background_filter::set_input_image( cv::Mat* src )
{
	this->mat_src_ = src;
}

void Background_filter::set_model_image( cv::Mat* src )
{
	this->mat_model_ = src;
}

cv::Mat* Background_filter::get_model_image()
{
	return mat_model_;
}
cv::Mat* Background_filter::get_diff()
{
	return mat_diff_;
}
cv::Mat* Background_filter::get_diff_grayscale()
{
	mat_diff_->convertTo( *mat_diff_gray_, CV_8U, 255.0 / 4000);
	return mat_diff_gray_;
}
cv::Mat* Background_filter::get_diff_binary()
{
	mat_diff_->convertTo( *mat_diff_gray_, CV_8U, 255.0 / 4000);
	cv::threshold(*mat_diff_gray_, *mat_diff_binary_, 1, 255, cv::THRESH_BINARY);
	return mat_diff_binary_;
}
cv::Mat* Background_filter::get_noise_mask()
{
	return mat_noise_mask_dilate_;
}
cv::Mat* Background_filter::get_noise_frequency()
{
	return mat_noise_frequency_;
}

void Background_filter::set_element_size(int element)
{
	this->element_size_ = element;
	int rect_len = element_size_*element_size_ + 1;
	*(this->element_) = element_mat_generator(rect_len, element_size_);
}

void Background_filter::compute()
{
	frame_count_++;
	if(frame_count_%50 == 0)
		frequency_cleaner_ = true;
	else 
		frequency_cleaner_ = false;

	for (int nRows = 0; nRows < mat_src_->rows; ++nRows )
	{
		ptr_src_	   = mat_src_->ptr< short>(nRows);
		ptr_model_	   = mat_model_->ptr< short>(nRows);
		ptr_diff_	   = mat_diff_->ptr< short>(nRows);
		ptr_mask_	   = mat_noise_mask_->ptr< uchar>(nRows);
		ptr_frequency_ = mat_noise_frequency_->ptr< short>(nRows);
		ptr_range_max_ = mat_noise_range_max_->ptr< short>(nRows);
		ptr_range_min_ = mat_noise_range_min_->ptr< short>(nRows);
		ptr_range_max_times = noise_max_calcutate_times_->ptr< short>(nRows);
		ptr_range_min_times = noise_min_calcutate_times_->ptr< short>(nRows);
		for (int nCols = 0; nCols < mat_src_->cols; ++nCols )
		{
			
			this->ptr_pos_ = nCols;
			// a. depth filter
			filter_by_depth();
			// b. training model
			if( ! stop_training_model_ )
				model_generator();
			// c. diff with background model
			filter_by_difference();
			noise_mask_generator();
			noise_mask_application();
		}
	}

	//*element_ = element_mat_generator( 7, 3);
	cv::dilate( *mat_noise_mask_, *mat_noise_mask_dilate_, *element_);
	cv::erode( *mat_diff_, *mat_diff_, *element_ );
	cv::blur( *mat_diff_, *mat_diff_,cv::Size( 2, 2 ));
	cv::dilate( *mat_diff_, *mat_diff_, *element_ );
}

void Background_filter::filter_by_depth()
{
	if ( ptr_src_[ptr_pos_] < 800 &&  ptr_src_[ptr_pos_] > 4000)
				ptr_src_[ptr_pos_] = 0;
}

void Background_filter::evaluate_threshold()
{
	// set threshlod by depth(cm)
	parm_threshold_ = (mfb_ * (ptr_src_[ptr_pos_]*0.1)*(ptr_src_[ptr_pos_]*0.1))/100;
	if(ptr_mask_[ptr_pos_] == 255)
		parm_threshold_ = parm_threshold_ * 2;
}
void Background_filter::filter_by_difference()
{
	// a. difference
	ptr_diff_[ptr_pos_] = abs( ptr_model_[ptr_pos_] - ptr_src_[ptr_pos_] );
	evaluate_threshold();
	// b. filter 
	if(frequency_cleaner_ && ptr_frequency_[ptr_pos_] < 10 )
		ptr_frequency_[ptr_pos_]  = 0;
	if(ptr_diff_[ptr_pos_] <= ptr_src_[ptr_pos_]*parm_threshold_)
		ptr_diff_[ptr_pos_] = 0;
	else
		ptr_frequency_[ptr_pos_] ++;
	if(ptr_frequency_[ptr_pos_] >= 15)
		ptr_mask_[ptr_pos_] = 255;
	else// no source aviliable
		ptr_mask_[ptr_pos_] = 0;

}

void Background_filter::model_generator()
{
	if ( ptr_src_[ptr_pos_] != 0 && ptr_model_[ptr_pos_] == 0 )
		ptr_model_[ptr_pos_] = ptr_src_[ptr_pos_];
	else if ( ptr_src_[ptr_pos_] != 0 && ptr_model_[ptr_pos_] != 0 )
		ptr_model_[ptr_pos_] = ( 1 - learning_rate_ ) * ptr_model_[ptr_pos_] + learning_rate_ * ptr_src_[ptr_pos_];
}

void Background_filter::noise_mask_generator()
{
	if ( ptr_range_max_[ptr_pos_] < ptr_src_[ptr_pos_] && ptr_range_max_times[ptr_pos_] < 20)
	{
		ptr_range_max_times[ptr_pos_]++;
		ptr_range_max_[ptr_pos_] = ptr_src_[ptr_pos_];
	}
	if ( ptr_range_min_[ptr_pos_] > ptr_src_[ptr_pos_] && ptr_range_min_times[ptr_pos_] < 20)
	{
		ptr_range_min_times[ptr_pos_]++;
		ptr_range_min_[ptr_pos_] = ptr_src_[ptr_pos_];
	}
}

void Background_filter::noise_mask_application()
{
	double local_threshold_ = (mfb_ * (ptr_range_max_[ptr_pos_]*0.1)*(ptr_range_max_[ptr_pos_]*0.1))/100;
	if(ptr_mask_[ptr_pos_] == 255)
		parm_threshold_ = parm_threshold_ * 2;

	if ( abs(ptr_src_[ptr_pos_]-ptr_range_max_[ptr_pos_])  <  ptr_range_max_[ptr_pos_] * local_threshold_)
	{
		//std::cout << "uncontinue 1" << std::endl;
		ptr_diff_[ptr_pos_] = 0;
	}

	local_threshold_ = (mfb_ * (ptr_range_min_[ptr_pos_]*0.1)*(ptr_range_min_[ptr_pos_]*0.1))/100;
	if(ptr_mask_[ptr_pos_] == 255)
		parm_threshold_ = parm_threshold_ * 2;
	if ( abs(ptr_src_[ptr_pos_]-ptr_range_min_[ptr_pos_])  <  ptr_range_min_[ptr_pos_] * local_threshold_)
	{
		std::cout << "uncontinue 2" << std::endl;
		ptr_diff_[ptr_pos_] = 0;
	}
}

cv::Mat Background_filter::element_mat_generator(int rect_len, int element_size)
{
	//cv::Mat* element = 
	return  cv::getStructuringElement( cv::MORPH_RECT, cv::Size( rect_len, rect_len), cv::Point( element_size, element_size) );
}

